$(document).ready(function(){
      $('.mads-li').owlCarousel({
        loop:true,
        margin:0,
        nav:false,
        items:1,
        dots:true,
        animateOut: 'fadeOut',
        autoplay:true,
        autoplayHoverPause:false
      });   

    $(window).scroll(function(){
      if ($(this).scrollTop() > 100) {
        $('.right-button').fadeIn(500)
      } else {
        $('.right-button').fadeOut(500)
      }
    });
    //gotop
    $('.right-button').click(function () {
      $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
    //append 最新消息模組
    $('.newslist').append($('.newsappend'));
    //left-menu
      $(".showbutton").click(function(){
        $(this).parent('.left-menu').toggleClass('active');
      })
      
        if ($(window).width() > 767) {

        //把控制項加入陣列       
        var fadein_index=[
                ".product-link .col-sm-6",
                ".newslist",
                ".foot"
                ];
        //滾動animation
        //當視窗高度到達物件時的動作
        function scrollanimation(a, b) {
            $(a).each(function() {
                // Check the location of each desired element //
                var objectBottom = $(this).offset().top + $(this).outerHeight();
                var windowBottom = $(window).scrollTop() + $(window).innerHeight() + 300;
                if (objectBottom < windowBottom) {
                    $(this).addClass(b);
                }
            });
        };

        //遇到class+animation
        $(window).on("load", function() {
              fadein_index.forEach(function(e){
              });       
            $(window).scroll(function() {
                //複製以下新增
                fadein_index.forEach(function(e){
                        scrollanimation(e,'animated fadeIn');
                });

            });
        });
    }
    //switch snc
    $('#cm11').hide();
    $('.switch a').click(function(e){
        e.preventDefault();
        var id = $(this).attr('href');
        $('.cm9').fadeOut();
        $('.cm11').fadeOut();
        $(id).fadeIn(500);
    })
    // 分類頁無連結移除
    $('.listBS a[href="#"]').click(function(e){
        e.preventDefault();
    })

    //emily fadeIn
    if ($(window).width() > 767) {
    //把控制項加入陣列       
    var fadein_index=[
            ".product-link .col-sm-6",
            ".newslist",
            ".foot"
            ];
    var scroll_fadeIn=[
            ".section-2",
            ".section-3 .col-sm-12",
            ".section-3 .col-sm-10:nth-of-type(1)",
            ".section-3 .col-sm-10:nth-of-type(2)",
            ".section-3 .col-sm-10:nth-of-type(3)",
            ".section-3 .col-sm-10:nth-of-type(4)",
            ".section-3 .col-sm-10:nth-of-type(5)",
            ".section-3 .col-sm-10:nth-of-type(6)",
            ".section-3 .col-xs-12:last-child",
            ".section-4",
            ".section-5"
    ]            
    //滾動animation
    //當視窗高度到達物件時的動作
    function scrollanimation(a, b) {
        $(a).each(function() {
            // Check the location of each desired element //
            var objectBottom = $(this).offset().top + $(this).outerHeight();
            var windowBottom = $(window).scrollTop() + $(window).innerHeight() + 300;
            if (objectBottom < windowBottom) {
                $(this).addClass(b);
            }
        });
    };                
    //當物件到達視窗200pxfadeIn
    function scroll_emily(a, b) {
        $(a).each(function() {
            // Check the location of each desired element //
            // var obj = $(this).scrollTop();
            var objectBottom = $(this).offset().top + $(this).outerHeight();
            var windowBottom = $(window).scrollTop() + $(window).innerHeight();
            if (objectBottom < windowBottom ) {
                $(this).addClass(b);
            }
        });
    };

    //遇到class+animation
    $(window).on("load", function() {
              fadein_index.forEach(function(e){
              });       
            $(window).scroll(function() {
                //複製以下新增


                scroll_fadeIn.forEach(function(e){
                    scroll_emily(e,'animated fadeIn')
                })

            });
        });
    }

  })